import React from "react";

export default function Navbar() {
  const [isActive, setisActive] = React.useState(false);

  return (
    <nav
      className="navbar is-transparent"
      role="navigation"
      aria-label="main navigation"
    >
      <div className="navbar-brand">
        <a href="/#" className="navbar-item">
          <img
            src="https://www.travelwiser.io/f99ca3fb784129d77bc112f0965073f8.png"
            alt="Logo"
            width="212"
            height="48"
          />
        </a>

        <a
          href="/#"
          onClick={() => {
            setisActive(!isActive);
          }}
          role="button"
          className={`navbar-burger burger ${isActive ? "is-active" : ""}`}
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarBasicExample"
        >
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>
      <div
        id="navbarBasicExample"
        className={`navbar-menu ${isActive ? "is-active" : ""}`}
      >
        <div className="navbar-end">
          <div className="navbar-item">
            <a href="/#" className="navbar-item">
              Find destination
            </a>
            <a href="/#" className="navbar-item">
              Plan your trip
            </a>
            <a href="/#" className="navbar-item">
              Blog
            </a>
            <a href="/#" className="navbar-item">
              Community
            </a>

            <div className="navbar-item has-dropdown is-hoverable">
              <a href="/#" className="navbar-link">
                More
              </a>
              <div className="navbar-dropdown">
                <a href="/#" className="navbar-item">
                  Ambassadors
                </a>
                <a href="/#" className="navbar-item">
                  Globetrotters
                </a>
              </div>
            </div>
            <div className="buttons">
              <a href="/#" className="button is-outlined">
                Login
              </a>
              <a href="/#" className="button is-danger is-focus">
                Sign up
              </a>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}
