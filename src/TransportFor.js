import React from "react";
import "./main.scss";

export default function TransportFor() {
  return (
    <div>
      <p>Are you planning the transport for</p>
      <div className="columns">
        <div className="column is-one-third">
          <label className="checkbox">
            <input
              type="checkbox"
              name="myCheckbox"
              value="1"
              onclick="selectOnlyThis(this)"
            />
            Yourself
          </label>
        </div>
        <div className="column is-one-third">
          <label className="checkbox">
            <input
              type="checkbox"
              name="myCheckbox"
              value="2"
              onclick="selectOnlyThis(this)"
            />
            A part of the group
          </label>
        </div>
        <div className="column is-one-third">
          <label className="checkbox">
            <input
              type="checkbox"
              name="myCheckbox"
              value="3"
              onclick="selectOnlyThis(this)"
            />
            Everyone
          </label>
        </div>
      </div>
    </div>
  );
}
