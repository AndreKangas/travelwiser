import React, { useState } from "react";
import "./main.scss";
import Steps from "./Steps";
import WithWhom from "./WithWhom";
import DatesDestination from "./DatesDestination";

export default function Form() {
  const [status, setStatus] = useState(0);

  return (
    <>
      <Steps status={status} />
      {status === 0 && (
        <>
          <WithWhom />
          <button
            className="button is-rounded is-danger"
            onClick={() => setStatus(1)}
          >
            Next
          </button>
        </>
      )}
      {status === 1 && (
        <>
          <DatesDestination />
          <button
            className="button is-rounded is-danger"
            onClick={() => setStatus(0)}
          >
            Back
          </button>
        </>
      )}
    </>
  );
}
