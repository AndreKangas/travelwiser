import React from "react";
import "./main.scss";

export default function NumbersOfPeople() {
  return (
    <div className="columns">
      <div className="column">
        <div className="select">
          <select>
            <option>0</option>
            <option>1</option>
            <option>2</option>
          </select>
        </div>
      </div>

      <div className="column">
        <div className="select">
          <select>
            <option>0</option>
            <option>1</option>
            <option>2</option>
          </select>
        </div>
      </div>

      <div className="column">
        <div className="select">
          <select>
            <option>0</option>
            <option>1</option>
            <option>2</option>
          </select>
        </div>
      </div>
    </div>
  );
}
