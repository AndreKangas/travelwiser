import React, { useState } from "react";
import "./main.scss";
import ChooseDatesDestination from "./ChooseDatesDestination";

export default function DtesDestination() {
  const [status, setStatus] = useState(0);

  return (
    <form className="container">
      <p>Dates &#38; Destination</p>
      <div className="notification">
        <div className="columns">
          <label className="radio column is-one-third">
            <input type="radio" name="rsvp" onChange={() => setStatus(1)} />
            Return
          </label>
          <label className="radio column is-one-third">
            <input type="radio" name="rsvp" onChange={() => setStatus(2)} />
            One way
          </label>
          <label className="radio column is-one-third">
            <input
              type="radio"
              name="rsvp"
              disabled
              onChange={() => setStatus(3)}
            />
            Multi-city
          </label>
        </div>

        {status === 1 && (
          <div>
            <ChooseDatesDestination />
          </div>
        )}
        {status === 2 && (
          <div>
            <ChooseDatesDestination />
          </div>
        )}
      </div>
    </form>
  );
}
