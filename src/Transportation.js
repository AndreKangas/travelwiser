import React from "react";
import "./main.scss";

export default function Transportation() {
  return (
    <div>
      <h1>How would you like to get there?</h1>

      <div className="buttons">
        <button
          className="button is-rounded column is-one-fifths"
          type="button"
        >
          <span className="icon has-text-danger">
            <i className="fas fa-plane"></i>
          </span>
        </button>
        <button
          className="button is-rounded column is-one-fifths"
          type="button"
        >
          <span className="icon has-text-danger">
            <i className="fas fa-train"></i>
          </span>
        </button>
        <button
          className="button is-rounded column is-one-fifths"
          type="button"
        >
          <span className="icon has-text-danger">
            <i className="fas fa-bus"></i>
          </span>
        </button>
        <button
          className="button is-rounded column is-one-fifths"
          type="button"
        >
          <span className="icon has-text-danger">
            <i className="fas fa-car"></i>
          </span>
        </button>
        <button
          className="button is-rounded column is-one-fifths"
          type="button"
        >
          <span className="icon has-text-danger">
            <i className="fas fa-ship"></i>
          </span>
        </button>
      </div>
    </div>
  );
}
