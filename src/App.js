import "./App.scss";
import Navbar from "./Navbar";
import Form from "./Form";
// import './main.scss';

function App() {
  return (
    <div className="App">
      <Navbar />
      <header className="App-header">
        <Form />
      </header>
    </div>
  );
}

export default App;
