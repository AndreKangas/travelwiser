import React, { useReducer, useState } from "react";
import "./main.scss";

export default function ChooseDatesDestination() {
  const [checked, toggle] = useReducer((checked) => !checked, false);
  const [departure, setDeparture] = useState("");
  const [arrival, setArrival] = useState("");
  return (
    <>
      <div className="columns">
        <div className="control column is-two-fifths has-icons-left">
          {checked ? (
            <input
              className="input is-medium"
              type="search"
              value={departure}
              onChange={(e) => setDeparture(e.target.value)}
              placeholder="Enter arrival"
            />
          ) : (
            <input
              className="input is-medium"
              type="search"
              value={arrival}
              onChange={(e) => setArrival(e.target.value)}
              placeholder="Enter arrival"
            />
          )}
          <span className="icon is-left">
            <i className="fas fa-search"></i>
          </span>
        </div>
        <span
          style={{ cursor: "pointer" }}
          className="icon has-text-info column is-one-fifths"
          onClick={toggle}
        >
          <i className="fas fa-arrows-alt-h"></i>
        </span>
        <div className="control column is-two-fifths has-icons-left">
          {checked ? (
            <input
              className="input is-medium"
              type="search"
              value={arrival}
              onChange={(e) => setArrival(e.target.value)}
              placeholder="Enter departure"
            />
          ) : (
            <input
              className="input is-medium"
              type="search"
              value={departure}
              onChange={(e) => setDeparture(e.target.value)}
              placeholder="Enter departure"
            />
          )}
          <span className="icon is-left">
            <i className="fas fa-search"></i>
          </span>
        </div>
      </div>
      <label className="checkbox">
        <input type="checkbox" name="myCheckbox" />
        Direct flights only
      </label>
      <br />
      <div className="columns">
        <div className="control column is-two-fifths has-icons-left">
          <input
            className="input is-medium"
            type="date"
            placeholder="Enter departure"
          />
          <span className="icon is-left">
            <i className="fas fa-search"></i>
          </span>
        </div>
        <span
          style={{ cursor: "pointer" }}
          className="icon has-text-info column is-one-fifths"
        >
          <i className="fas fa-arrow-right"></i>
        </span>
        <div className="control column is-two-fifths has-icons-left">
          <input
            className="input is-medium"
            type="date"
            placeholder="Enter departure"
          />
          <span className="icon is-left">
            <i className="fas fa-search"></i>
          </span>
        </div>
      </div>
    </>
  );
}
