import React, { useState } from "react";
import "./main.scss";
import Transportation from "./Transportation";
import NumbersOfPeople from "./NumbersOfPeople";
import TransportFor from "./TransportFor";
import "@fortawesome/fontawesome-free/css/all.css";

export default function WithWhom() {
  const [status, setStatus] = useState(0);

  return (
    <form className="container">
      <p>With Whom?</p>
      <div className="notification">
        <div className="columns">
          <label className="radio column is-one-fifths">
            <input type="radio" name="rsvp" onChange={() => setStatus(1)} />
            Solo
          </label>
          <label className="radio column is-one-fifths">
            <input type="radio" name="rsvp" onChange={() => setStatus(2)} />
            Friends/Family
          </label>
          <label className="radio column is-one-fifths">
            <input
              type="radio"
              name="rsvp"
              disabled
              onChange={() => setStatus(3)}
            />
            Group
          </label>
          <span className="icon has-text-info column is-two-fifths">
            <i className="fas fa-info-circle"></i>
          </span>
        </div>

        {status === 1 && (
          <div>
            <Transportation />
          </div>
        )}
        {status === 2 && (
          <div>
            <NumbersOfPeople />
            <Transportation />
            <TransportFor />
          </div>
        )}
      </div>
    </form>
  );
}
