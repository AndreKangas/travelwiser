import React from "react";

export default function Steps(props) {
  return (
    <>
      <div className="columns">
        <div className="column">
          <button className="button is-inverted">
            <span>
              Step 1<br />
              <p>With whom?</p>
            </span>
          </button>
          <div className="columns">
            <div className="column">
              {props.status === 0 && (
                <div>
                  <progress
                    className="progress is-danger"
                    value="100"
                    max="100"
                  ></progress>
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="column">
          <button className="button is-inverted">
            <span>
              Step 2<br />
              <p>Dates &#38; Destination</p>
            </span>
          </button>
          <div className="columns">
            <div className="column">
              {props.status === 1 && (
                <div>
                  <progress
                    className="progress is-danger"
                    value="100"
                    max="100"
                  ></progress>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
